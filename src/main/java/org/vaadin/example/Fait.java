package org.vaadin.example;

public class Fait {
    private String fait;

    public Fait(String fait){
        this.fait = fait;
    }
    public String getFait() {
        return fait;
    }

    public void setFait(String fait) {
        this.fait = fait;
    }

}

